package dev.edward.blog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import dev.edward.blog.helper.SessionManager;

public class ProfileActivity extends AppCompatActivity {


    private ImageView back_btn, header_image;
    private CircleImageView profile_image;
    private TextView followers_count, following_count, profile_name, error;
    private Button follow_btn;
    private RecyclerView post_list;
    private String user_id, name, photo_url;
    private FloatingActionButton add;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //Finding view by ID
        back_btn = findViewById(R.id.back_btn);
        header_image = findViewById(R.id.header_image);
        profile_image = findViewById(R.id.profile_image);
        followers_count = findViewById(R.id.followers_count);
        following_count = findViewById(R.id.following_count);
        profile_name = findViewById(R.id.profile_name);
        follow_btn = findViewById(R.id.follow_btn);
        add = findViewById(R.id.add);

        sessionManager = SessionManager.getInstance(ProfileActivity.this);

        //Initializing add button
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, PostActivity.class));
            }
        });

        //Setting user details
        setUserDetails();
    }


    private void setUserDetails() {

        final String userId = sessionManager.getUserId();
        name = sessionManager.getUserDetails().getName();
        photo_url = sessionManager.getUserDetails().getPhoto();

        profile_name.setText(name);
        Glide.with(ProfileActivity.this)
                .applyDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .load(photo_url)
                .into(profile_image);

        Glide.with(ProfileActivity.this)
                .applyDefaultRequestOptions(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .load(photo_url)
                .into(header_image);
    }

}
