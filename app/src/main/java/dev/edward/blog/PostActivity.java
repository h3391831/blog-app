package dev.edward.blog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import dev.edward.blog.helper.SessionManager;
import dev.edward.blog.model.Article;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostActivity extends AppCompatActivity {

    private final String TAG = PostActivity.class.getSimpleName();

    private Uri post_image_uri;
    private ImageView post_image;
    private EditText post_title, post_details;
    private ProgressDialog pdialog;
    private static final int PICK_IMAGE = 1;
    public static final int TAKE_PHOTO = 2;

    private Bitmap selectedImage;
    OkHttpClient okHttpClient;

    SessionManager sessionManager;

    private static final String UPLOAD_ARTICLE_API = "/api/blogs/store";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        //Finding View By Id
        TextView header = findViewById(R.id.header);
        CircleImageView profileimage = findViewById(R.id.profileimage);
        ImageView search = findViewById(R.id.search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        post_title = findViewById(R.id.post_title);
        post_details = findViewById(R.id.post_details);
        post_image = findViewById(R.id.post_image);
        Button submit_post_btn = findViewById(R.id.submit_post_btn);

        okHttpClient = new OkHttpClient();
        sessionManager = SessionManager.getInstance(PostActivity.this);

        submit_post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allClear()){
                    uploadPost();
                }
            }
        });

        //Customizing toolbar
//        Typeface pacifico = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
        header.setText("Post Blog");
//        header.setTypeface(pacifico);
        profileimage.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        //Setting post image to imageview
        post_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(PostActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PostActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    } else {
                        pickImage();
                    }
                } else {
                    pickImage();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {

            if (data != null) {
                post_image_uri = data.getData();
                post_image.setImageURI(post_image_uri);

                try {
                    if (android.os.Build.VERSION.SDK_INT >= 29) {
                        // To handle deprication use
                        selectedImage = ImageDecoder.decodeBitmap(ImageDecoder.createSource(this.getContentResolver(), post_image_uri));
                    } else {
                        // Use older version
                        selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), post_image_uri);
                    }

                    Log.d(TAG, "pick photo");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Unable to load Image", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == TAKE_PHOTO) {
            Log.d(TAG, "take photo");
            if (data != null) {
                Log.d(TAG, "set data");
                selectedImage = (Bitmap) data.getExtras().get("data");
                post_image.setImageBitmap(selectedImage);
            } else {
                Toast.makeText(this, "Unable to load Image", Toast.LENGTH_LONG).show();
            }

        }
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    private boolean allClear() {
        if (post_image_uri == null) {
            toast("Please select an Image");
            return false;
        }else if (TextUtils.isEmpty(post_title.getText().toString())){
            post_title.setError("Title can't be Empty");
            return false;
        }else if (TextUtils.isEmpty(post_details.getText().toString())){
            post_details.setError("Detail can't be Empty");
            return false;
        }else {
            return true;
        }
    }

    private void uploadPost() {
        pdialog = new ProgressDialog(PostActivity.this, R.style.MyAlertDialogStyle);
        pdialog.setMessage("Please wait...");
        pdialog.setIndeterminate(true);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setCancelable(false);
        pdialog.show();

        savePostDetails();
    }

    private void savePostDetails() {
        String title = post_title.getText().toString();
        String details = post_details.getText().toString();
        String desc = details.substring(0, Math.min(details.length(), 250));
//        String user = mAuth.getCurrentUser().getUid();

        Article article = new Article();
        article.setUser(sessionManager.getUserDetails());
        article.setViews(0);
        article.setTime(System.currentTimeMillis());
        article.setTitle(title);
        article.setDesc(desc);
        article.setDetails(details);

        sendRegisterRequest(article);
    }

    private void sendRegisterRequest(Article article) {
        pdialog.show();
        //create a file to write bitmap data
        File file = getImageFile();

        RequestBody formBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("photo", file.getName(),
                        RequestBody.create(file, MediaType.parse("image/png")))
                .addFormDataPart("title", article.getTitle())
                .addFormDataPart("token", article.getUser().getToken())
                .addFormDataPart("content", article.getDetails())
                .addFormDataPart("user_id", article.getUser().getId())
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + UPLOAD_ARTICLE_API)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onPostFailed(e.getMessage());

                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "response not success ");
                    onPostFailed("error code is " + response.code());
                    return;
                }

                String responseStr = response.body().string();
                Log.d(TAG, "response = " + responseStr);


                try {
                    final JSONObject responseObj = new JSONObject(responseStr);

                    if (responseObj.getInt("success") == 0) {
                        String errorMsg = responseObj.getString("error_msg");
                        Log.d(TAG, "Register failed, error is " + errorMsg);

                        onPostFailed(errorMsg);
                        return;
                    }

                    JSONObject articleObj = responseObj.getJSONObject("blog");
                    OnPostSuccess(articleObj);

                } catch (JSONException e) {
                    Log.d(TAG, "failed parse json");
                    e.printStackTrace();
                }
            }
        });
    }

    private void onPostFailed(final String errorMsg) {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                pdialog.dismiss();
                Toast.makeText(PostActivity.this, errorMsg, Toast.LENGTH_LONG).show();
            }

        });
    }

    @NotNull
    private File getImageFile() {
        File file = new File(this.getCacheDir(), "myImage.jpg");
        try {
            file.createNewFile();
            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] bitmapData = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            Log.d(TAG, "file occur IOException");
        }
        return file;
    }

    public void OnPostSuccess(final JSONObject article) {
        runOnUiThread(new Runnable() {
            public void run() {
                pdialog.dismiss();
                post_title.setText("");
                post_details.setText("");
                post_image.setImageResource(R.color.grey);
                Toast.makeText(PostActivity.this, "Post uploaded successfully :)", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void toast(String message){
        Toast.makeText(PostActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
