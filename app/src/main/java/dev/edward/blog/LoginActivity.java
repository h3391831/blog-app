package dev.edward.blog;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import dev.edward.blog.helper.SessionManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = LoginActivity.class.getSimpleName();

    private TextView toptext, conditions;
    private EditText email, password;
    private Button login;
    private ProgressDialog pdialog;
    private final static int RC_SIGN_IN = 2;
    private final String LOGIN_API_URL = "/api/login";

    OkHttpClient okHttpClient;

    // Session Manager Class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toptext = findViewById(R.id.toptext);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        conditions = findViewById(R.id.conditions);

        String text = "By signing in, you accept our <a href='#'>Terms and Conditions</a><br>& <a href='#'>Privacy Policy</a>";
        conditions.setText(Html.fromHtml(text));
        conditions.setClickable(true);

        okHttpClient = new OkHttpClient();
        session = SessionManager.getInstance(this);

        pdialog = new ProgressDialog(LoginActivity.this, R.style.MyAlertDialogStyle);
        pdialog.setMessage("Please wait...");
        pdialog.setIndeterminate(true);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInWithMail();
            }
        });
    }

    private void signInWithMail(){
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();

        if (TextUtils.isEmpty(userEmail)){
            email.setError("Please enter your email");
        } else if (!userEmail.contains("@") || !userEmail.contains(".")){
            email.setError("Please enter a valid Email");
        } else if (TextUtils.isEmpty(userPassword)){
            password.setError("Please enter your password");
        } else {
            pdialog.show();
        }

        sendLoginRequest(userEmail, userPassword);
    }

    private void sendLoginRequest(String email, String password) {
        RequestBody body = new FormBody.Builder()
                .add("email", email)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + LOGIN_API_URL)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                pdialog.dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                pdialog.dismiss();
                try {
                    JSONObject responseObj = new JSONObject(response.body().string());
                    Log.i(TAG, "responseObj: " + responseObj);

                    JSONObject userObj = responseObj.getJSONObject("user");

                    // Creating user login session
                    session.createLoginSession("" + userObj.getInt("id"), userObj.toString());

                    startActivity();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

//        Log.d(TAG, "LoginActivity startActivity");
    }

}
