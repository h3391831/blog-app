package dev.edward.blog;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;
import dev.edward.blog.helper.SessionManager;

public class WelcomeScreenActivity extends AppCompatActivity {

    TextView topText, quoteText;
    Button signUp, signIn;

    // Session Manager Class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 呼叫父類別onCreate
        super.onCreate(savedInstanceState);
        // 設定對應的layout
        setContentView(R.layout.activity_welcome_screen);
        // window大小不再不受手機屏幕大小限制，即window可能超出屏幕之外，這時部分內容在屏幕之外。
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        // 透過findViewById找到對應的UI元件
        signIn = findViewById(R.id.signin);
        signUp = findViewById(R.id.signup);
        quoteText = findViewById(R.id.quotetext);
        topText = findViewById(R.id.toptext);

        // 設定onClick時所要執行的動作
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeScreenActivity.this, LoginActivity.class));
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeScreenActivity.this, RegisterActivity.class));
            }
        });

        showQuotes();
    }

    @Override
    protected void onStart() {
        super.onStart();

        session = SessionManager.getInstance(this);

        if (session.isLoggedIn()) {
            Intent intent = new Intent(WelcomeScreenActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void showQuotes(){
        String quote1 ="Nothing is better than going home to family and eating good food and relaxing";
        String quote2 ="One cannot think well, love well, sleep well, if one has not dined well";
        String quote3 ="Your diet is a bank account. Good food choices are good investments";
        String quote4 ="Spaghetti can be eaten most successfully if you inhale it like a vacuum cleaner";
        String quote5 ="If music be the food of love, play on";

        String[] quotes = {quote1,quote2,quote3,quote4,quote5};
        Random r = new Random();
        int n = r.nextInt(5);
        quoteText.setText("\""+quotes[n]+"\"");

    }

}
