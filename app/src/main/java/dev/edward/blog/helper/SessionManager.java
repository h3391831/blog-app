package dev.edward.blog.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import org.json.JSONObject;

import dev.edward.blog.model.User;

public class SessionManager {

    private final String TAG = SessionManager.class.getSimpleName();
    // Sharedpref file name
    private static final String PREF_NAME = "BlogAppPref";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_ID = "user_id";
    // User name (make variable public to access from outside)
    public static final String KEY_USER = "user";

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    User user;

    private static SessionManager INSTANCE = null;

    // other instance variables can be here

    private SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SessionManager(context);
        }

        return INSTANCE;
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String id, String userJsonObjStr) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_ID, id);

        // Storing user in pref
        editor.putString(KEY_USER, userJsonObjStr);

        // commit changes
        editor.commit();
    }

    public String getUserId() {
        return pref.getString(KEY_ID, null);
    }

    /**
     * Get stored session data
     * */
    public User getUserDetails() {
        if (user == null) {
            user = new User();
        }

        String userJsonObjStr = pref.getString(KEY_USER, null);
        if (userJsonObjStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(userJsonObjStr);
                user.setId("" + jsonObj.getInt("id"));
                user.setName(jsonObj.getString("name"));
                user.setEmail(jsonObj.getString("email"));
                user.setPhoto(jsonObj.getString("photo"));
                user.setToken(jsonObj.getString("api_token"));

                Log.d(TAG, "session get user detail:" + jsonObj.toString());
            } catch (Throwable t) {
                Log.e(TAG, "Could not parse malformed JSON: " + userJsonObjStr);
            }
        }

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser() {
        user = null;

        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
