package dev.edward.blog.model;

import dev.edward.blog.BuildConfig;

public class User {

    private String id;
    private String name;
    private String email;
    private String password;
    private String photo;
    private String token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        if (photo.contains("http://") || photo.contains("https://")) {
            this.photo = photo;
        } else {
            this.photo = BuildConfig.SERVER_URL + "/" + photo;
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
