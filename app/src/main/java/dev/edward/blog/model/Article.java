package dev.edward.blog.model;

import dev.edward.blog.BuildConfig;

public class Article {

    private int Views;
    private String Image;
    private long Time;
    private String Title;
    private String Desc;
    private String Details;
    private User user;

    public Article(){}

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getViews() {
        return Views;
    }

    public void setViews(int views) {
        Views = views;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        if (image.contains("http://") || image.contains("https://")) {
            Image = image;
        } else {
            Image = BuildConfig.SERVER_URL + "/" + image;
        }
    }

    public long getTime() {
        return Time;
    }

    public void setTime(long time) {
        Time = time;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

}