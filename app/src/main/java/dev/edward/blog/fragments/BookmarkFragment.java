package dev.edward.blog.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.edward.blog.BuildConfig;
import dev.edward.blog.R;
import dev.edward.blog.adapter.BookmarkAdapter;
import dev.edward.blog.model.Bookmark;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarkFragment extends Fragment {

    private final String TAG = BookmarkFragment.class.getSimpleName();

    private RecyclerView articleList;
    private BookmarkAdapter adapter;
    private CoordinatorLayout coordinatorLayout;
    private String userId;
    private RelativeLayout error;

    private static final String GET_BOOKMARK_API = "/api/blogs/list";

    private ArrayList<Bookmark> mArrayList;
    OkHttpClient okHttpClient;

    public BookmarkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmark, container, false);

        okHttpClient = new OkHttpClient();

        userId = "3";
        error = view.findViewById(R.id.error);
        error.setVisibility(View.GONE);

        //Initializing the Recycler View
        articleList = view.findViewById(R.id.article_list);
        populateArticleList();

        return view;
    }

    // Populating Article List
    private void populateArticleList(){
        RequestBody body = new FormBody.Builder()
                .add("user_id", userId)
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + GET_BOOKMARK_API)
                .get()
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject responseObj = new JSONObject(response.body().string());
                    Log.i(TAG, "responseObj: " + responseObj);

                    JSONArray itemJsonArr = responseObj.getJSONArray("blogs");
                    mArrayList = new ArrayList<>();
                    for (int i = 0; i < itemJsonArr.length(); i++) {
                        Bookmark bookmark = new Bookmark();
                        JSONObject articleJsonObj = itemJsonArr.getJSONObject(i);
                        bookmark.setTitle(articleJsonObj.getString("title"));
                        bookmark.setImage(articleJsonObj.getString("cover"));
                        bookmark.setDesc(articleJsonObj.getString("content"));
                        mArrayList.add(bookmark);
                    }

                    adapter = new BookmarkAdapter(mArrayList, getActivity());

                    //Checking if no bookmarks
                    if (adapter.getItemCount() == 0){
                        error.setVisibility(View.VISIBLE);
                    } else {
                        error.setVisibility(View.GONE);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                articleList.setAdapter(adapter);
                                articleList.setHasFixedSize(true);
                                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                                articleList.setLayoutManager(linearLayoutManager);
                            }
                        });
                    }

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
