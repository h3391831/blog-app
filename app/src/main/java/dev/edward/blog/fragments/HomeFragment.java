package dev.edward.blog.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.edward.blog.BuildConfig;
import dev.edward.blog.LoginActivity;
import dev.edward.blog.R;
import dev.edward.blog.adapter.ArticleAdapter;
import dev.edward.blog.helper.SessionManager;
import dev.edward.blog.model.Article;
import dev.edward.blog.model.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class HomeFragment extends Fragment {

    private final String TAG = LoginActivity.class.getSimpleName();

    private RecyclerView articleList;
//    private FirestoreRecyclerAdapter<Article, ArticleViewHolder> adapter;
    private ArticleAdapter adapter;
    private CoordinatorLayout coordinatorLayout;

    private static final String GET_ARTICLE_API = "/api/blogs/list";

    private ArrayList<Article> mArrayList;
    OkHttpClient okHttpClient;
    SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        okHttpClient = new OkHttpClient();
        session = SessionManager.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Initializing the Recycler View
        articleList = view.findViewById(R.id.blog_list);
        coordinatorLayout = view.findViewById(R.id.main_layout);

        populateArticleList();

        return view;
    }


    // Populating Article List
    private void populateArticleList(){
        RequestBody body = new FormBody.Builder()
                .add("user_id", session.getUserId())
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + GET_ARTICLE_API)
                .get()
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject responseObj = new JSONObject(response.body().string());
                    Log.i(TAG, "responseObj: " + responseObj);

                    JSONArray articleJsonArr = responseObj.getJSONArray("blogs");
                    mArrayList = new ArrayList<>();
                    for (int i = 0; i < articleJsonArr.length(); i++) {
                        Article article = new Article();
                        User user = new User();
                        JSONObject articleJsonObj = articleJsonArr.getJSONObject(i);
                        article.setTitle(articleJsonObj.getString("title"));
                        article.setImage(articleJsonObj.getString("cover_img"));
                        article.setDesc(articleJsonObj.getString("content"));

                        JSONObject userJsonObj = articleJsonObj.getJSONObject("user");
                        user.setId(""+userJsonObj.getInt("id"));
                        user.setName(userJsonObj.getString("name"));
                        user.setPhoto(userJsonObj.getString("photo"));
                        user.setEmail(userJsonObj.getString("email"));

                        article.setUser(user);
                        mArrayList.add(article);
                    }

                    adapter = new ArticleAdapter(mArrayList, getActivity());

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            articleList.setAdapter(adapter);
                            articleList.setHasFixedSize(true);
                            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                            articleList.setLayoutManager(linearLayoutManager);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
