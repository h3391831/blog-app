package dev.edward.blog;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.edward.blog.adapter.ArticleAdapter;
import dev.edward.blog.model.Article;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SearchActivity extends AppCompatActivity {

    private final String TAG = SearchActivity.class.getSimpleName();
    CoordinatorLayout coordinatorLayout;
    Toolbar toolbar;
    ImageView back;
    EditText searchInput;
    RecyclerView articleList;

    private ArticleAdapter adapter;
    private ArrayList<Article> mArrayList;
    private static final String SEARCH_ARTICLE_API = "/api/user/prescriptions";
    OkHttpClient okHttpClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Finding view by id
        coordinatorLayout = findViewById(R.id.constraint_layout);
        toolbar = findViewById(R.id.toolbar);
        back = findViewById(R.id.back);
        searchInput = findViewById(R.id.search_input);
        articleList = findViewById(R.id.blog_list);

        okHttpClient = new OkHttpClient();

        //Customizing the action bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        searchInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (searchInput.getRight() - searchInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        searchInput.setText("");
                    }
                }
                return false;
            }
        });

        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String text = searchInput.getText().toString();
                    if (!text.equals("")){
                        populateArticleList(text);
                    }
                }
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.super.onBackPressed();
            }
        });
    }

    // Populating Article List
    private void populateArticleList(String searchText){
        RequestBody body = new FormBody.Builder()
                .add("user_id", "3")
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + SEARCH_ARTICLE_API)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject responseObj = new JSONObject(response.body().string());
                    Log.i(TAG, "responseObj: " + responseObj);

                    JSONArray articleJsonArr = responseObj.getJSONArray("prescriptions");
                    mArrayList = new ArrayList<>();
                    for (int i = 0; i < articleJsonArr.length(); i++) {
                        Article article = new Article();
                        JSONObject articleJsonObj = articleJsonArr.getJSONObject(i);
                        article.setTitle(articleJsonObj.getString("hospital_name"));
                        article.setImage(articleJsonObj.getString("image_url"));
                        article.setDesc(articleJsonObj.getString("detected_json_result"));
                        mArrayList.add(article);
                    }

                    adapter = new ArticleAdapter(mArrayList, SearchActivity.this);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            articleList.setAdapter(adapter);
                            articleList.setHasFixedSize(true);
                            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchActivity.this);
                            articleList.setLayoutManager(linearLayoutManager);
                        }
                    });

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
