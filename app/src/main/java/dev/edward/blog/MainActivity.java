package dev.edward.blog;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import de.hdodenhof.circleimageview.CircleImageView;
import dev.edward.blog.fragments.HomeFragment;
import dev.edward.blog.fragments.WebviewFragment;
import dev.edward.blog.helper.SessionManager;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();

    TextView header;
    BottomNavigationView bottom_nav;
    private CircleImageView profileimage;
    private Fragment homeFragment,webviewFragment,bookmarksFragment,activityFragment;

    boolean doubleBackToExitPressedOnce = false;

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = SessionManager.getInstance(this);

        //Customizing the Toolbar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = session.getUserId();
            String channelName = session.getUserDetails().getName();
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        if( task.getResult() == null)
                            return;
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        Log.i(TAG,"firebase token " + token);
                    }
                });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
//        Typeface pacifico = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
        header = findViewById(R.id.header);
//        header.setTypeface(pacifico);
        profileimage = findViewById(R.id.profileimage);
        profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile = new Intent(MainActivity.this, ProfileActivity.class);
                profile.putExtra("UserId", "3");
                startActivity(profile);
            }
        });
//        loadprofileImage(mAuth.getCurrentUser().getPhotoUrl());
        ImageView search = findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
            }
        });


        //Customizing Bottom Navigation
        bottom_nav = findViewById(R.id.bottom_nav);
//        BottomNavigationViewHelper.disableShiftMode(bottom_nav);
//        Menu m = bottom_nav.getMenu();
//        for (int i=0;i<m.size();i++) {
//            MenuItem mi = m.getItem(i);
//            applyFontToMenuItem(mi);
//        }
        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Initializing Home Fragment
        homeFragment = new HomeFragment();
        webviewFragment = new WebviewFragment();
//        bookmarksFragment = new BookmarkFragment();
//        activityFragment = new ActivityFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, homeFragment)
                .add(R.id.fragment_container,webviewFragment)
//                .add(R.id.fragment_container,bookmarksFragment)
//                .add(R.id.fragment_container,activityFragment)
                .hide(webviewFragment)
//                .hide(bookmarksFragment)
//                .hide(activityFragment)
                .commit();
        
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    replaceFragment(homeFragment, webviewFragment);
                    return true;
                case R.id.webview:
                    replaceFragment(webviewFragment, homeFragment);
                    return true;
                case R.id.post:
                    startActivity(new Intent(MainActivity.this, PostActivity.class));
                    return true;
//                case R.id.bookmarks:
//                    replaceFragment(bookmarksFragment, homeFragment,webviewFragment,activityFragment);
//                    return true;
//                case R.id.activity:
//                    replaceFragment(activityFragment,homeFragment,webviewFragment,bookmarksFragment);
//                    return true;
                case R.id.setting:
                    startActivity(new Intent(MainActivity.this, SettingActivity.class));
                    return true;
            }
            return false;
        }
    };

    private void applyFontToMenuItem(MenuItem mi) {
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
//        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void loadprofileImage(Uri uri){
//        Glide.with(MainActivity.this)
//                .applyDefaultRequestOptions(new RequestOptions()
//                        .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
//                        .error(R.drawable.com_facebook_profile_picture_blank_square)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL))
//                .load(uri)
//                .into(profileimage);
    }

    private void replaceFragment(Fragment one, Fragment two){
        if (!one.isVisible()){
            getSupportFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .show(one)
                    .hide(two)
//                    .hide(three)
//                    .hide(four)
                    .commit();
        }

    }
}
