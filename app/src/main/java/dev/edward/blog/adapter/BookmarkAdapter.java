package dev.edward.blog.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import dev.edward.blog.R;
import dev.edward.blog.helper.BookmarksViewHolder;
import dev.edward.blog.model.Bookmark;


public class BookmarkAdapter extends RecyclerView.Adapter<BookmarksViewHolder> {
    private String TAG = BookmarkAdapter.class.getSimpleName();

    public ArrayList<Bookmark> mArrayList;
    private Context mContext;
    public BookmarksViewHolder viewHolder;

    public BookmarkAdapter(ArrayList<Bookmark> arrayList, Context context) {
        mContext = context;
        mArrayList = arrayList;
    }

    @Override
    public BookmarksViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_row, viewGroup, false);
        viewHolder = new BookmarksViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BookmarksViewHolder viewHolder, int i) {
        viewHolder.setTitle(mArrayList.get(i).getTitle());
        viewHolder.setImage(mArrayList.get(i).getImage(), mContext);
        viewHolder.setDesc(mArrayList.get(i).getDesc());
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

}
