package dev.edward.blog.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import dev.edward.blog.R;
import dev.edward.blog.helper.ArticleViewHolder;
import dev.edward.blog.helper.SessionManager;
import dev.edward.blog.model.Article;


public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {
    private String TAG = ArticleAdapter.class.getSimpleName();

    public ArrayList<Article> mArrayList;
    private Context mContext;
    public ArticleViewHolder viewHolder;
    SessionManager sessionManager;

    public ArticleAdapter(ArrayList<Article> arrayList, Context context) {
        mContext = context;
        sessionManager = SessionManager.getInstance(mContext);
        mArrayList = arrayList;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_row, viewGroup, false);
        viewHolder = new ArticleViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ArticleViewHolder viewHolder, int i) {
        Article article = mArrayList.get(i);
        viewHolder.setTitle(article.getTitle());
        viewHolder.setImage(article.getImage(), mContext);
        viewHolder.setDesc(article.getDesc());
        viewHolder.setUser(article.getUser(), mContext);
        viewHolder.showPostDetails(article, (Activity) mContext);
    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

}
