package dev.edward.blog;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import dev.edward.blog.helper.SessionManager;
import dev.edward.blog.model.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = RegisterActivity.class.getSimpleName();

    private TextView topText, conditions;
    private Button signUp;
    private CircleImageView avatar;
    private EditText fullName,email,password,cnfPassword;
    private ProgressDialog pDialog;
    public static final int PICK_IMAGE = 1;
    public static final int TAKE_PHOTO = 2;

    private Uri mainImageUri;
    private Bitmap selectedImage;
    private final String REGISTER_API_URL = "/api/register";

    OkHttpClient okHttpClient;

    // Session Manager Class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        okHttpClient = new OkHttpClient();
        session = SessionManager.getInstance(this);

        avatar = findViewById(R.id.avatar);
        fullName = findViewById(R.id.fullname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        cnfPassword = findViewById(R.id.cnfpassword);
        signUp = findViewById(R.id.signup);
        topText = findViewById(R.id.toptext);
        conditions = findViewById(R.id.conditions);

        String text = "By signing up, you accept our <a href='#'>Terms and Conditions</a><br>& <a href='#'>Privacy Policy</a>";
        conditions.setText(Html.fromHtml(text));
        conditions.setClickable(true);
        conditions.setMovementMethod(LinkMovementMethod.getInstance());

        pDialog = new ProgressDialog(RegisterActivity.this, R.style.MyAlertDialogStyle);
        pDialog.setMessage("Signing up...");
        pDialog.setIndeterminate(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    }else{
                        pickImage();
                    }
                } else {
                    pickImage();
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = createUser();
                if (user == null) {
                    return;
                }

                if (selectedImage == null) {
                    Log.d(TAG, "image null");
                    return;
                }

                sendRegisterRequest(user);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_IMAGE) {
            if (data != null) {
                mainImageUri = data.getData();
                avatar.setImageURI(mainImageUri);

                try {
                    if (android.os.Build.VERSION.SDK_INT >= 29) {
                        // To handle deprication use
                        selectedImage = ImageDecoder.decodeBitmap(ImageDecoder.createSource(this.getContentResolver(), mainImageUri));
                    } else {
                        // Use older version
                        selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mainImageUri);
                    }

                    Log.d(TAG, "pick photo");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, "Unable to load Image", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == TAKE_PHOTO) {
            Log.d(TAG, "take photo");
            if (data != null) {
                Log.d(TAG, "set data");
                selectedImage = (Bitmap) data.getExtras().get("data");
                avatar.setImageBitmap(selectedImage);
            } else {
                Toast.makeText(this, "Unable to load Image", Toast.LENGTH_LONG).show();
            }

        }
    }

    private User createUser(){
        final String name = fullName.getText().toString();
        final String mail = email.getText().toString();
        final String pwd = password.getText().toString();
        final String cnfPwd = cnfPassword.getText().toString();
        if (!checkFields(name, mail, pwd, cnfPwd)){
            return null;
        }

        User user = new User();
        user.setName(name);
        user.setEmail(mail);
        user.setPassword(pwd);

        return user;
    }

    private boolean checkFields(String name, String mail, String pwd, String cnfpwd){

        if (TextUtils.isEmpty(name)){
            fullName.setError("Name can't be empty");
        }

        if (TextUtils.isEmpty(mail)){
            email.setError("Email can't be empty");
        }

        if (TextUtils.isEmpty(pwd)){
            password.setError("Please enter a password");
        }

        if (TextUtils.isEmpty(cnfpwd)){
            cnfPassword.setError("Please confirm your password");
        } else if (name.length()<3){
            fullName.setError("Name must be of at least 3 characters");
        } else if (!mail.contains("@") || !mail.contains(".")){
            email.setError("Please enter a valid email");
        } else if (pwd.length() < 6){
            password.setError("Password must be of at least 5 characters");
        } else if (!cnfpwd.matches(pwd)){
            password.setError("Passwords don't match !");
            cnfPassword.setError("Passwords don't match !");
        } else {
            return true;
        }

        return false;
    }

    private void pickImage(){
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("image/*");
//        startActivityForResult(intent, PICK_IMAGE);


        final CharSequence[] options = { "Take Photo", "Choose from Gallery", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, TAKE_PHOTO);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , PICK_IMAGE);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void sendRegisterRequest(User user) {
        pDialog.show();
        //create a file to write bitmap data
        File file = getImageFile();

        RequestBody formBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("photo", file.getName(),
                        RequestBody.create(file, MediaType.parse("image/png")))
                .addFormDataPart("email", user.getEmail())
                .addFormDataPart("name", user.getName())
                .addFormDataPart("password", user.getPassword())
                .build();

        Request request = new Request.Builder()
                .url(BuildConfig.SERVER_URL + REGISTER_API_URL)
                .post(formBody)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onRegisterFailed(e.getMessage());

                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "response not success ");
                    onRegisterFailed("error code is " + response.code());
                    return;
                }

                String responseStr = response.body().string();
                Log.d(TAG, "response = " + responseStr);


                try {
                    final JSONObject responseObj = new JSONObject(responseStr);

                    if (responseObj.getInt("success") == 0) {
                        String errorMsg = responseObj.getString("error_msg");
                        onRegisterFailed(errorMsg);
                        Log.d(TAG, "Register failed, error is " + errorMsg);
                    }

                    final JSONObject userObj = responseObj.getJSONObject("user");
                    // Creating user login session
                    session.createLoginSession("" + userObj.getInt("id"), userObj.toString());

                    onRegisterSuccess();
                } catch (JSONException e) {
                    Log.d(TAG, "failed parse json");
                    e.printStackTrace();
                }
            }
        });
    }

    private void onRegisterSuccess() {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                pDialog.dismiss();
                Toast.makeText(RegisterActivity.this, "success", Toast.LENGTH_LONG).show();

                startActivity();
            }

        });
    }

    private void onRegisterFailed(final String errorMsg) {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                pDialog.dismiss();
                Toast.makeText(RegisterActivity.this, errorMsg , Toast.LENGTH_LONG).show();
            }

        });
    }

    @NotNull
    private File getImageFile() {
        File file = new File(this.getCacheDir(), "myImage.jpg");
        try {
            file.createNewFile();
            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] bitmapData = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
           Log.d(TAG, "file occur IOException");
        }
        return file;
    }

    private void startActivity(){
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

//        Log.d(TAG, "LoginActivity startActivity");
    }
}
